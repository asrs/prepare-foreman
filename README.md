Prepare-foreman Machine
=========

This roles is here to prepare a machine for a Foreman / Satellite Installation.

Requirements
------------

CentOs / Red Hat OS

default requirement: 4 vCPU | 20 Gio of ram | a second disk with at least 500 Gio

Role Variables
--------------
- **installation_product**: Choose between 'foreman' or 'satellite'
- **diskpath**: the path of the second disk were the lvm should be instanciate
- **hostname**: the hostname of the configurated machine
- **varlibpulp**: size of created partition /var/lib/pulp
- **varcachepulp**: size of created partition /var/cache/pulp
- **varlibmongodb**: size of created partition for /var/lib/mongodb
- **cpu_requirement**: number of minimal cpu / vcpu needed
- **ram_requirement**: number of minimal memory needed
- **disk_requirement**: number of minimal size of diskpath

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

- hosts: servers
  roles:
  - role: prepare-foreman
	vars:
		hostname: sat.labcr.local
		diskpath: /dev/vdb
		varlibpulp_size: 10G
		varcachepulp_size: 3G
		varlibmongodb_size: 2G
		cpu_requirement: 2
		ram_requirement: 800
		disk_requirement: 20000

License
-------

BSD

Author Information
------------------

Clement Richard for STARTX <clement.richard@startx.fr>
